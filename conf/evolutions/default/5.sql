
# --- !Ups

 --- Nueva tabla categoria

CREATE TABLE category (
    user varchar(255) NOT NULL, 
    name varchar(255) NOT NULL,
    constraint fk_user FOREIGN KEY (user) REFERENCES user_task (login),
    CONSTRAINT pk_category PRIMARY KEY (user, name)
);

 --- Nueva tabla categoria-tarea relacion muchos a muchos

CREATE TABLE category_task (
    user varchar(255) NOT NULL, 
    category varchar(255) NOT NULL,
    id_task  integer NOT NULL,
    constraint fk_category FOREIGN KEY (user, category) REFERENCES category (user, name),    
    constraint fk_task FOREIGN KEY (id_task) REFERENCES task (id),
    CONSTRAINT pk_category_task primary key(user, category, id_task)
);


# --- !Downs

DROP TABLE category;

DROP TABLE category_task;
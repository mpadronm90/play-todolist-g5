package controllers

import play.api._
import play.api.mvc._
import play.api.data._
import play.api.data.Forms._
import models._

import java.util.Date
import java.text.SimpleDateFormat

// JSON

import play.api.mvc._
import play.api.libs.json._
import play.api.libs.functional.syntax._

    
object Application extends Controller {

   val dateWrite = Writes.dateWrites("yyyy-MM-dd")
   val formatter = new SimpleDateFormat("yyyy-MM-dd")

   implicit val locationWrites: Writes[Task] = (
      (JsPath \ "id").write[Long] and
      (JsPath \ "label").write[String] and
      (JsPath \ "taskOwner").write[String] and
      (JsPath \ "deadline").writeNullable[Date](dateWrite)
   )(unlift(Task.unapply))

   val taskForm = Form(
      "label" -> nonEmptyText     
   )

   def index = Action {
      Ok(views.html.index(Task.all(), taskForm))
   }

   def tasks = Action {
      Ok(Json.toJson(Task.all("anonymous")))
   }

   def tasksUserToday(user: String) = tasksUser(user, Some(formatter.format(new Date)))

   def tasksUser(user: String, dateStr: Option[String]) = Action {
      if (User.exists(user)) {
         val date = dateStr match {
            case Some(dateStr) => Some(formatter.parse(dateStr))
            case None => None
         }
         Ok(Json.toJson(Task.all(user, date)))
      }
      else BadRequest("Error: No existe el propietario de la tarea: " + user)
   }

   def findTask(id: Long) = Action {
      val task:Option[Task] = Task.findById(id)
      task match {
         case Some(t) => Ok(Json.toJson(t))
         case None => NotFound
      }
   }

   def newTask = newTaskUser("anonymous")

   def newTaskUser(user: String) = Action { implicit request =>
     taskForm.bindFromRequest.fold(
       errors => BadRequest("Error en la peticion"),
       label => if (User.exists(user)) {
                   val id: Long = Task.create(label, user)
                   Created(Json.toJson(Task.findById(id)))
                }
                else BadRequest("Error: No existe el propietario de la tarea: " + user)
     )
   }

   def deleteTask(id: Long) = Action {
     if (Task.delete(id)) Ok(views.html.index(Task.all(), taskForm)) else NotFound
   }

   def newCategory(login: String) = Action {  implicit request =>

       taskForm.bindFromRequest.fold(
       errors => BadRequest("Error en la peticion"),
       label => if (User.exists(login)) {
               
                  try{
                     var created = Category.create(login, label)
                     created match {
                       case true => Created("Categoria "+label+" ha sido creada correctamente en "+login)
                       case false => Conflict("La categoria "+label+" ya existe en "+login) 
                     }
                     }
                     catch{
                           case t : org.h2.jdbc.JdbcSQLException => Conflict("La categoria "+label+" ya existe en "+login) 
                          }   
                  
                }
                else BadRequest("Error: No existe el usuario: " + login)
     )   

   }

   def addTaskInCategory(login: String, category: String, id: Long) = Action { implicit request =>
   
   if (User.exists(login)) {
   
   try{

    val add: Boolean = Category.addTask(login, category, id)      
    add match {
      case true => Created("Se ha añadido la tarea "+id+" correctamente a la categoria "+category)
      case false => BadRequest("Peticion erronea")
    }
       }  catch {
                  case t: org.h2.jdbc.JdbcSQLException => Conflict("La tarea "+id+" ya existe en "+category+" para el usuario "+login) 
                }
      } else BadRequest("Error: No existe el usuario: " + login)

    }

    def modify(login: String, category: String, id: Long) = Action { implicit request =>

     taskForm.bindFromRequest.fold(
       errors => BadRequest("Error en la peticion"),
       label => if (User.exists(login)) {
         val existTask: Boolean = Category.existsTask(login, category, id)
         existTask match {
          case false => BadRequest("Error: No existe la tarea: " + id + "en el usuario "+login)  
          case true => {
            val existNew: Boolean =  Category.exists(login, label)
            existNew match {
              case false => { 
                if(Category.create(login, label)) {
                  Category.modify(login, category, id, label)             
                  Created("Se ha modificado la categoria "+category+" por "+label+ " en la tarea "+ id+ " del usuario "+login)
                }

                else Conflict("Nos e pudo modificar a categoria "+label+" en el usuario "+login+" para la tarea "+id)
              }
              case true => {
               Category.modify(login, category, id, label)              
               Created("Se ha modificado la categoria "+category+" por "+label+ " en la tarea "+ id+ " del usuario "+login)
               
             }
           }
         }


       }

       }else BadRequest("Error: No existe el usuario: " + login)  
       )

}
      
      def tasksCategory(login: String, category: String) = Action {
        if (User.exists(login)) {
          if(Category.exists(login, category)) {
            Ok(Json.toJson(Category.tasks(login, category)))           
       } else BadRequest("Error: No existe la categoria: " + category)
      }
      else BadRequest("Error: No existe usuario: " + login)

      }




       
}
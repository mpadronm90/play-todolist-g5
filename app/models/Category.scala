package models
import play.api.db._
import play.api.Play.current
import anorm._
import anorm.SqlParser._

object Category {

   def create(user: String, name: String): Boolean = DB.withConnection { implicit c =>
         SQL("insert into category (user, name) values ({user}, {name})").on(
               'user -> user,
               'name -> name
            ).executeInsert()

     } match {
       case _: Option[Long] => true
       case None => false
     }
     

  def exists(user: String, name: String): Boolean = DB.withConnection { implicit c =>
      SQL("select count(*) from category where user = {user} and name = {name}").on(
          'user -> user, 'name -> name).as(scalar[Long].single) == 1
   }

   def addTask(user: String, category: String, id_task: Long): Boolean = DB.withConnection { implicit c => 
         SQL("insert into category_task (user, category, id_task) values ({user}, {category}, {id_task})").on(
            'user -> user,
            'category -> category,
            'id_task -> id_task
            ).executeInsert()

   } match {
       case _: Option[Long] => true
       case None => false
     }

    def existsTask(user: String, category: String, id_task: Long): Boolean = DB.withConnection { implicit c =>
      SQL("select count(*) from category_task where user = {user} and category = {category} and id_task={id_task}").on(
          'user -> user, 'category -> category, 'id_task -> id_task).as(scalar[Long].single) == 1
   }

   def modify(user: String, category: String, id_task: Long, modify: String): Boolean = DB.withConnection { implicit c =>
        val upd: Int =SQL("update category_task set category = {modify} where user = {user} and category = {category} and id_task = {id_task}").on(
                'user -> user, 'category -> category, 'id_task -> id_task, 'modify -> modify).executeUpdate()
              upd match {
                case 1 => true
                case _ => false
              }
   }

    def modifyCategory(user: String, category: String, id_task: Long, modify: String): Boolean = DB.withConnection { implicit c =>
      val existTask: Boolean = Category.existsTask(user, category, id_task)
      existTask match {
        case false => false
        case true => {
          val existNew: Boolean =  Category.exists(user, modify)
          existNew match {
            case false => { 
              if(Category.create(user, modify)) {
              Category.modify(user, category, id_task, modify)
              }           
            else false
          }
            case true => {
             Category.modify(user, category, id_task, modify)
            }
          }
          
      }
      
    }
  }

  def tasks(user: String, category: String): List[Task] = DB.withConnection { implicit c =>
    SQL("Select id, label, task_owner, deadline from task t, category_task c where t.id = c.id_task and c.user = user and c.category = category").on(
      'user -> user,
      'category -> category).as(Task.task *)
  }

 
  


} 